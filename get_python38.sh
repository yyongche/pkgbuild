#!/bin/bash

## sudo /vagrant/get_python39.sh

apt-get clean
apt-get update

apt-get install -y --download-only software-properties-common

add-apt-repository -y ppa:deadsnakes/ppa
apt-get update

apt-get install -y --download-only python3.8 python3.8-venv python3.8-dev
apt-get install -y --download-only libc6:i386
apt-get install -y --download-only libc6-dbg

cp /var/cache/apt/archives/*.deb /vagrant/python38/
