#!/bin/bash

## ./get_packages.sh
##
## To install *.snap on an offline machine
## snap ack <snapname>.assert
## snap install <snapname>.snap
## To install *.deb on an offline machine
## dpkg -i <debname>.deb

pushd packages
rm *.assert
rm *.snap
rm *.deb

snap download audacity
snap download ffmpeg
snap download gimp

apt download google-chrome-stable

popd
