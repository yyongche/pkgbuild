#!/bin/bash

## podman run -it --rm -v .:/app centos:7 bash
## /app/build_centos7.sh -b -- # get OS dependencies
## /app/build_centos7.sh -k -- # build Kaldi
## /app/build_centos7.sh -3 -- # get Python 3 dependencies
## /app/build_centos7.sh -2 -- # get Python 2 dependencies
## /app/build_centos7.sh -p -- # get Perl dependencies

while getopts "bk32p" OPTION; do
    case $OPTION in
    b)
        yum install -y epel-release
        rpm -v --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro
        rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm

        mkdir /home/yum
        yum install -y --downloadonly --downloaddir=/home/yum zlib atlas icu
        yum install -y --downloadonly --downloaddir=/home/yum sox ffmpeg
        yum install -y --downloadonly --downloaddir=/home/yum expat

        mkdir -p /home/os/centos7
        cd /home/os/centos7
        ls -1 /home/yum/*.rpm | xargs -L 1 -I {} sh -c "rpm2cpio {} | cpio -idmv"
        cd /home && tar -zcvf os.tar.gz os/ && mv os.tar.gz /app/centos7
        ;;
    k)
        yum install -y gcc gcc-c++ kernel-devel make
        yum install -y zlib-devel bzip2 automake autoconf wget git libtool subversion
        yum install -y atlas-devel python python3 gawk which patch

        bash /app/kaldi/make_cpu.sh
        cd /home && tar -zcvf kaldi_cpu.tar.gz kaldi/ && mv kaldi_cpu.tar.gz /app/centos7

        rm -r /home/kaldi/
        rm -r /home/kaldi_v3/

        rpm -i /app/cuda-repo-centos7-9.2.148-1.x86_64.rpm
        yum install -y cuda-toolkit-9-2

        bash /app/kaldi/make_gpu.sh
        cd /home && tar -zcvf kaldi_gpu.tar.gz kaldi/ && mv kaldi_gpu.tar.gz /app/centos7
        ;;
    3)
        yum install -y python3 python3-devel python3-pip
        pip3 install --no-cache-dir --upgrade pip
        pip3 install --no-cache-dir pipenv

        export LC_ALL=en_US.UTF-8
        export LANG=en_US.UTF-8

        cd /app/python3 && pipenv install -r requirements.txt
        rm /app/python3/Pipfile
        rm /app/python3/Pipfile.lock

        envdir=`ls -1 /root/.local/share/virtualenvs | grep python3`
        cd /root/.local/share/virtualenvs/${envdir} && \
        tar -zcvf python3.tar.gz lib/ lib64/ && mv python3.tar.gz /app/centos7
        ;;
    2)
        yum install -y epel-release
        yum install -y python python-pip
        pip install --no-cache-dir --upgrade pip
        pip install --no-cache-dir pipenv

        cd /app/python2 && pipenv install -r requirements.txt
        rm /app/python2/Pipfile
        rm /app/python2/Pipfile.lock

        envdir=`ls -1 /root/.local/share/virtualenvs | grep python2`
        cd /root/.local/share/virtualenvs/${envdir} && \
        tar -zcvf python2.tar.gz lib/ lib64/ && mv python2.tar.gz /app/centos7
        ;;
    p)
        yum install -y gcc gcc-c++ kernel-devel make curl perl
        yum install -y perl-ExtUtils-MakeMaker perl-Test-Simple
        yum install -y expat-devel

        curl -L https://cpanmin.us | perl - App::cpanminus
        cpanm --local-lib=/root/perl5 local::lib
        eval $(perl -I /root/perl5/lib/perl5/ -Mlocal::lib)

        cpanm XML::Simple

        cd /root && tar -zcvf perl5.tar.gz perl5/ && mv perl5.tar.gz /app/centos7
        ;;
    *)
        exit 1
        ;;
    esac
done

exit 0
