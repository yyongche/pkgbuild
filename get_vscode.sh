#!/bin/bash

pushd vscode

pipenv --python 3.6 && pipenv run pip download pipenv && \
    pipenv --rm
rm Pipfile Pipfile.lock

apt download code

read -r -d "" EXTENSIONS << EOM
Angular.ng-template@13.3.0
dbaeumer.vscode-eslint@2.2.2
eamodio.gitlens@12.0.5
EditorConfig.EditorConfig@0.16.4
esbenp.prettier-vscode@9.3.0
gurumukhi.selected-lines-count@1.4.0
ms-azuretools.vscode-docker@1.21.0
ms-python.python@2022.2.1924087327
ms-python.vscode-pylance@2022.3.2
ms-toolsai.jupyter@2022.2.1030672458
ms-toolsai.jupyter-keymap@1.0.0
ms-toolsai.jupyter-renderers@1.0.6
nrwl.angular-console@17.14.1
rangav.vscode-thunder-client@1.12.5
vscodevim.vim@1.22.2
EOM

for EXT in ${EXTENSIONS[@]} ; do
    IFS=@; read -r EXTENSION VERSION <<< "$EXT"
    IFS=.; read -r PUBLISHER NAME <<< "$EXTENSION"
    unset IFS

    URL="https://${PUBLISHER}.gallery.vsassets.io/_apis/public/gallery/publisher/${PUBLISHER}/extension/${NAME}/${VERSION}/assetbyname/Microsoft.VisualStudio.Services.VSIXPackage"
    curl -L $URL -o ${EXT}.vsix
done

popd
