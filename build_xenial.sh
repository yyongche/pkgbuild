#!/bin/bash

## podman run -it --rm -v .:/app ubuntu:16.04 bash
## /app/build_xenial.sh -b -- # get OS dependencies
## /app/build_xenial.sh -k -- # build Kaldi
## /app/build_xenial.sh -3 -- # get Python 3 dependencies
## /app/build_xenial.sh -2 -- # get Python 2 dependencies
## /app/build_xenial.sh -p -- # get Perl dependencies

while getopts "bk32p" OPTION; do
    case $OPTION in
    b)
        cp /app/jonathonf-ubuntu-ffmpeg-4-xenial.list /etc/apt/sources.list.d/
        apt-key adv --keyserver keyserver.ubuntu.com --recv-keys F06FC659
        apt-get update

        apt-get clean
        apt-get update
        apt-get install -y --download-only zlib1g libatlas3-base icu-devtools
        apt-get install -y --download-only sox libsox-fmt-all ffmpeg
        apt-get install -y --download-only libexpat1

        mkdir -p /home/os/xenial
        ls -1 /var/cache/apt/archives/*.deb | xargs -L 1 -I {} dpkg -x {} /home/os/xenial
        cd /home && tar -zcvf os.tar.gz os/ && mv os.tar.gz /app/xenial
        ;;
    k)
        apt-get update
        apt-get install -y build-essential
        apt-get install -y zlib1g-dev automake autoconf wget git libtool subversion
        apt-get install -y libatlas3-base python2.7 python3 gawk

        bash /app/kaldi/make_cpu.sh
        cd /home && tar -zcvf kaldi_cpu.tar.gz kaldi/ && mv kaldi_cpu.tar.gz /app/xenial

        rm -r /home/kaldi/
        rm -r /home/kaldi_v3/

        dpkg -i /app/cuda-repo-xenial-9.2.148-1_amd64.deb
        apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
        apt-get update
        apt-get install -y cuda-toolkit-9-2

        bash /app/kaldi/make_gpu.sh
        cd /home && tar -zcvf kaldi_gpu.tar.gz kaldi/ && mv kaldi_gpu.tar.gz /app/xenial
        ;;
    3)
        apt-get update
        apt-get install -y python3 python3-pip
        pip3 install --no-cache-dir --upgrade pip
        pip3 install --no-cache-dir pipenv

        export LC_ALL=C.UTF-8
        export LANG=C.UTF-8

        cd /app/python3 && pipenv install -r requirements.txt
        rm /app/python3/Pipfile
        rm /app/python3/Pipfile.lock

        envdir=`ls -1 /root/.local/share/virtualenvs | grep python3`
        cd /root/.local/share/virtualenvs/${envdir} && \
        tar -zcvf python3.tar.gz lib/ && mv python3.tar.gz /app/xenial
        ;;
    2)
        apt-get update
        apt-get install -y python python-pip
        pip install --no-cache-dir --upgrade pip
        pip install --no-cache-dir pipenv

        cd /app/python2 && pipenv install -r requirements.txt
        rm /app/python2/Pipfile
        rm /app/python2/Pipfile.lock

        envdir=`ls -1 /root/.local/share/virtualenvs | grep python2`
        cd /root/.local/share/virtualenvs/${envdir} && \
        tar -zcvf python2.tar.gz lib/ && mv python2.tar.gz /app/xenial
        ;;
    p)
        apt-get update
        apt-get install -y build-essential curl perl
        apt-get install -y libexpat1-dev

        curl -L https://cpanmin.us | perl - App::cpanminus
        cpanm --local-lib=/root/perl5 local::lib
        eval $(perl -I /root/perl5/lib/perl5/ -Mlocal::lib)

        cpanm XML::Simple

        cd /root && tar -zcvf perl5.tar.gz perl5/ && mv perl5.tar.gz /app/xenial
        ;;
    *)
        exit 1
        ;;
    esac
done

exit 0
