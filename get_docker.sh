#!/bin/bash

## sudo /vagrant/get_docker.sh

apt-get clean
apt-get update

apt-get install -y --download-only apt-transport-https ca-certificates curl
apt-get install -y --download-only gnupg-agent software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update

apt-get install -y --download-only docker-ce docker-ce-cli containerd.io

distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list \
    | tee /etc/apt/sources.list.d/nvidia-docker.list
apt-get update

apt-get install -y --download-only nvidia-docker2
apt-get install -y --download-only nvidia-container-runtime

cp /var/cache/apt/archives/*.deb /vagrant/docker/

curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" \
    -o /vagrant/docker/docker-compose
chmod +x /vagrant/docker/docker-compose

mkdir -p /vagrant/docker/cli-plugins
curl -SL "https://github.com/docker/compose/releases/download/v2.2.3/docker-compose-linux-x86_64" \
    -o /vagrant/docker/cli-plugins/docker-compose
chmod +x /vagrant/docker/cli-plugins/docker-compose
