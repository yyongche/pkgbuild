#!/bin/bash

set -e

if [ $# -ne 1 ]; then
    echo "Download and install given rnnlm version from rnnlm.org"
    echo
    echo "Usage: $0 <rnnlm_ver> # e.g. $0 rnnlm-0.4b"
    exit 1
fi

rnnlm_ver=$1
tools_dir="$(readlink -f "$(dirname "$0")/../")"

if [ "$(basename "$tools_dir")" != "tools" ]; then
    echo "Cannot find tools/ dir. Am I in tools/extras?"
    exit 1
fi

cd $tools_dir
echo Downloading and installing the rnnlm tools
# http://www.fit.vutbr.cz/~imikolov/rnnlm/$rnnlm_ver.tgz
arc_site="http://www.fit.vutbr.cz/~imikolov/rnnlm"
if [ "$rnnlm_ver" == "rnnlm-0.4b" ]; then
    arc_site="https://ftp.fau.de/macports/distfiles/rnnlm"
fi
arc_file="$rnnlm_ver.tgz"
if [ ! -f "$arc_file" ]; then
    wget "$arc_site/$rnnlm_ver.tgz" -O "$arc_file" || exit 1;
fi
if [ "$rnnlm_ver" == "rnnlm-0.4b" ]; then
    tar -xvzf $rnnlm_ver.tgz || exit 1;
    cd $rnnlm_ver
else
    mkdir $rnnlm_ver
    cd $rnnlm_ver
    tar -xvzf ../$rnnlm_ver.tgz || exit 1;
fi
patch  < ../extras/mikolov_rnnlm.patch
make CC=g++ || exit 1;
echo Done making the rnnlm tools
