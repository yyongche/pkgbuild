#!/bin/bash

cd /home
tar -zxvf /app/kaldi/kaldi_v3.tar.gz

cd kaldi_v3/tools
make

./extras/install_ffv.sh
./extras/install_irstlm.sh
./extras/install_faster_rnnlm.sh
cp /app/kaldi/install_mikolov_rnnlm.sh extras/install_mikolov_rnnlm.sh
./extras/install_mikolov_rnnlm.sh rnnlm-0.4b
cp /app/kaldi/srilm-1.7.1.tar.gz srilm.tgz
./install_srilm.sh

cd ../src
if [ "$#" -eq 1 ]; then
    if [ "$1" = "bionic" ]; then
        cp /app/kaldi/configure configure
        ./configure --use-cuda=no
    fi
else
    ./configure --use-cuda=no
fi
make

mkdir /home/kaldi
cp .version /home/kaldi/
find . -executable -type f -print | xargs -I {} cp --parents {} /home/kaldi
rm /home/kaldi/configure
rm -r /home/kaldi/doc
rm -r /home/kaldi/probe

cd ../tools
find . -executable -type f -name "sph2pipe" -print | xargs -I {} cp {} /home/kaldi/base
find pitch_trackers -executable -type f -print | xargs -I {} cp {} /home/kaldi/base
cp faster-rnnlm/faster-rnnlm/rnnlm /home/kaldi/base/rnnlm
cp rnnlm-0.4b/rnnlm /home/kaldi/base/mikolov_rnnlm

mkdir /home/kaldi/lib
find . -name "*.so*" -not -path "*/.libs/*" -print | xargs -I {} cp -P {} /home/kaldi/lib
mkdir /home/kaldi/openfstbin
find openfst/bin -executable -type f -print | xargs -I {} cp {} /home/kaldi/openfstbin
mkdir /home/kaldi/sctkbin
find sctk/bin -executable -type f -print | xargs -I {} cp {} /home/kaldi/sctkbin
mkdir /home/kaldi/irstlmbin
find irstlm/bin -executable -type f -print | xargs -I {} cp {} /home/kaldi/irstlmbin
mkdir /home/kaldi/srilmbin
cd srilm/bin && find . -executable -type f -not -name "*.~*" -print | \
xargs -I {} cp --parents {} /home/kaldi/srilmbin
