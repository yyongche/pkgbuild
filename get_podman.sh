#!/bin/bash

## sudo /vagrant/get_podman.sh

apt-get clean
apt-get update

apt-get install -y --download-only uidmap

distribution=$(. /etc/os-release;echo ${ID^}_${VERSION_ID})
echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/x$distribution/ /" \
    | tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/x$distribution/Release.key \
    | apt-key add -
apt-get update

apt-get install -y --download-only podman buildah skopeo

cp /var/cache/apt/archives/*.deb /vagrant/podman/
