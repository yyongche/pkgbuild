#!/bin/bash

## podman run -it --rm -v .:/app ubuntu:18.04 bash
## /app/build_bionic.sh -b -- # get OS dependencies
## /app/build_bionic.sh -k -- # build Kaldi
## /app/build_bionic.sh -g -- # build Kaldi with GPU
## /app/build_bionic.sh -3 -- # get Python 3 dependencies
## /app/build_bionic.sh -2 -- # get Python 2 dependencies
## /app/build_bionic.sh -p -- # get Perl dependencies

while getopts "bkg32p" OPTION; do
    case $OPTION in
    b)
        apt-get clean
        apt-get update
        apt-get install -y --download-only zlib1g libatlas3-base icu-devtools
        apt-get install -y --download-only sox libsox-fmt-all ffmpeg
        apt-get install -y --download-only libexpat1

        mkdir -p /home/os/bionic
        ls -1 /var/cache/apt/archives/*.deb | xargs -L 1 -I {} dpkg -x {} /home/os/bionic
        cd /home && tar -zcvf os.tar.gz os/ && mv os.tar.gz /app/bionic
        ;;
    k)
        apt-get update
        apt-get install -y build-essential
        apt-get install -y zlib1g-dev automake autoconf wget git libtool subversion
        apt-get install -y libatlas3-base python2.7 python3 gawk

        bash /app/kaldi/make_cpu.sh bionic
        cd /home && tar -zcvf kaldi_cpu.tar.gz kaldi/ && mv kaldi_cpu.tar.gz /app/bionic
        ;;
    g)
        apt-get update
        apt-get install -y build-essential
        apt-get install -y zlib1g-dev automake autoconf wget git libtool subversion
        apt-get install -y libatlas3-base python2.7 python3 gawk

        dpkg -i /app/cuda-repo-bionic-10.2.89-1_amd64.deb
        apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
        apt-get update
        apt-get install -y cuda-toolkit-10-2

        bash /app/kaldi/make_gpu.sh bionic
        cd /home && tar -zcvf kaldi_gpu.tar.gz kaldi/ && mv kaldi_gpu.tar.gz /app/bionic
        ;;
    3)
        apt-get update
        apt-get install -y python3 python3-pip
        pip3 install --no-cache-dir --upgrade pip
        pip3 install --no-cache-dir pipenv

        export LC_ALL=C.UTF-8
        export LANG=C.UTF-8

        cd /app/python3 && pipenv install -r requirements.txt
        rm /app/python3/Pipfile
        rm /app/python3/Pipfile.lock

        envdir=`ls -1 /root/.local/share/virtualenvs | grep python3`
        cd /root/.local/share/virtualenvs/${envdir} && \
        tar -zcvf python3.tar.gz lib/ && mv python3.tar.gz /app/bionic
        ;;
    2)
        apt-get update
        apt-get install -y python python-pip
        pip install --no-cache-dir --upgrade pip
        pip install --no-cache-dir pipenv

        cd /app/python2 && pipenv install -r requirements.txt
        rm /app/python2/Pipfile
        rm /app/python2/Pipfile.lock

        envdir=`ls -1 /root/.local/share/virtualenvs | grep python2`
        cd /root/.local/share/virtualenvs/${envdir} && \
        tar -zcvf python2.tar.gz lib/ && mv python2.tar.gz /app/bionic
        ;;
    p)
        apt-get update
        apt-get install -y build-essential curl perl
        apt-get install -y libexpat1-dev

        curl -L https://cpanmin.us | perl - App::cpanminus
        cpanm --local-lib=/root/perl5 local::lib
        eval $(perl -I /root/perl5/lib/perl5/ -Mlocal::lib)

        cpanm XML::Simple

        cd /root && tar -zcvf perl5.tar.gz perl5/ && mv perl5.tar.gz /app/bionic
        ;;
    *)
        exit 1
        ;;
    esac
done

exit 0
